object fZadanie: TfZadanie
  Left = 47
  Top = 132
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1056#1072#1089#1095#1077#1090' '#1074#1086#1079#1074#1099#1096#1077#1085#1080#1103' '#1085#1072#1088#1091#1078#1085#1086#1075#1086' '#1088#1077#1083#1100#1089#1072' '#1074' '#1082#1088#1080#1074#1099#1093
  ClientHeight = 493
  ClientWidth = 953
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 953
    Height = 493
    ActivePage = TabSheet1
    Align = alClient
    TabIndex = 0
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #1047#1072#1076#1072#1085#1080#1077' '#1085#1072' '#1088#1072#1089#1095#1077#1090
      object Bevel13: TBevel
        Left = 520
        Top = 191
        Width = 103
        Height = 42
        Shape = bsFrame
      end
      object Bevel12: TBevel
        Left = 419
        Top = 211
        Width = 103
        Height = 25
        Shape = bsFrame
      end
      object Bevel11: TBevel
        Left = 318
        Top = 211
        Width = 103
        Height = 25
        Shape = bsFrame
      end
      object Bevel10: TBevel
        Left = 318
        Top = 191
        Width = 204
        Height = 22
        Shape = bsFrame
      end
      object Bevel9: TBevel
        Left = 243
        Top = 191
        Width = 77
        Height = 43
        Shape = bsFrame
      end
      object Bevel8: TBevel
        Left = 158
        Top = 191
        Width = 87
        Height = 43
        Shape = bsFrame
      end
      object Bevel7: TBevel
        Left = 93
        Top = 191
        Width = 67
        Height = 42
        Shape = bsFrame
      end
      object Bevel6: TBevel
        Left = 9
        Top = 191
        Width = 86
        Height = 42
        Shape = bsFrame
      end
      object CGauge1: TCGauge
        Left = 716
        Top = 107
        Width = 171
        Height = 24
        ForeColor = clGreen
        BackColor = clInfoBk
      end
      object Label1: TLabel
        Left = 664
        Top = 304
        Width = 32
        Height = 13
        Caption = 'Label1'
        Visible = False
      end
      object Bevel1: TBevel
        Left = 8
        Top = 11
        Width = 80
        Height = 39
        Shape = bsFrame
      end
      object Bevel2: TBevel
        Left = 86
        Top = 11
        Width = 235
        Height = 39
        Shape = bsFrame
      end
      object Label3: TLabel
        Left = 110
        Top = 23
        Width = 157
        Height = 16
        Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1079#1072#1076#1072#1085#1080#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Bevel3: TBevel
        Left = 319
        Top = 11
        Width = 146
        Height = 39
        Shape = bsFrame
      end
      object Label4: TLabel
        Left = 346
        Top = 23
        Width = 54
        Height = 16
        Caption = #1059#1095#1072#1089#1090#1086#1082
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Bevel4: TBevel
        Left = 463
        Top = 11
        Width = 75
        Height = 39
        Shape = bsFrame
      end
      object Label5: TLabel
        Left = 482
        Top = 23
        Width = 27
        Height = 16
        Caption = #1050' '#1087#1085
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Bevel5: TBevel
        Left = 536
        Top = 11
        Width = 97
        Height = 39
        Shape = bsFrame
      end
      object Label6: TLabel
        Left = 538
        Top = 23
        Width = 88
        Height = 16
        Caption = #1055#1086#1077#1079#1076#1086#1087#1086#1090#1086#1082
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label2: TLabel
        Left = 26
        Top = 14
        Width = 55
        Height = 32
        Caption = #8470' '#1079#1072#1076#1072#1085#1080#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label7: TLabel
        Left = 26
        Top = 196
        Width = 48
        Height = 32
        Caption = #8470' '#1087#1086#1077#1079#1076#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label8: TLabel
        Left = 99
        Top = 196
        Width = 56
        Height = 32
        Caption = #1050#1086#1083'-'#1074#1086' '#1087#1086#1077#1079#1076#1086#1074
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label9: TLabel
        Left = 162
        Top = 196
        Width = 79
        Height = 32
        Caption = #1058#1080#1087' '#1083#1086#1082#1086#1084#1086#1090#1080#1074#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label10: TLabel
        Left = 250
        Top = 196
        Width = 48
        Height = 32
        Caption = #1042#1077#1089' '#1087#1086#1077#1079#1076#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label11: TLabel
        Left = 354
        Top = 193
        Width = 131
        Height = 16
        Caption = #1056#1072#1079#1076#1077#1083#1100#1085#1099#1077' '#1087#1091#1085#1082#1090#1099
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label12: TLabel
        Left = 334
        Top = 214
        Width = 72
        Height = 16
        Caption = #1085#1072#1095#1072#1083#1100#1085#1099#1081
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label13: TLabel
        Left = 437
        Top = 214
        Width = 64
        Height = 16
        Caption = #1082#1086#1085#1077#1095#1085#1099#1081
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label14: TLabel
        Left = 535
        Top = 196
        Width = 68
        Height = 32
        Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1103' '#1089#1086#1089#1090#1072#1074#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label44: TLabel
        Left = 720
        Top = 136
        Width = 168
        Height = 16
        Caption = '________________________'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object DBGrid2: TDBGrid
        Left = 8
        Top = 231
        Width = 632
        Height = 224
        DataSource = MainData.s_poezd
        Options = [dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'npoezd'
            Width = 71
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'id_poezd'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'kol_vo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'rtiploco'
            Width = 85
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'rves'
            Width = 74
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'rname_rp1'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'rname_rp2'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'rkategor'
            Width = 100
            Visible = True
          end>
      end
      object DBGrid1: TDBGrid
        Left = 8
        Top = 48
        Width = 644
        Height = 137
        Ctl3D = True
        DataSource = MainData.s_zadrasch
        Options = [dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'id_uch'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'id_put'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'id_zadan'
            Title.Caption = #8470' '#1079#1072#1076#1072#1085#1080#1103
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'name'
            Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1079#1072#1076#1072#1085#1080#1103
            Width = 232
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'n_uch'
            Title.Caption = #1059#1095#1072#1089#1090#1086#1082
            Width = 145
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'k_ponij'
            Title.Caption = #1050' '#1087#1085
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'id_ppotok'
            PickList.Strings = (
              '1'
              '2')
            Title.Caption = #1055#1086#1077#1079#1076#1086#1087#1086#1090#1086#1082
            Width = 95
            Visible = True
          end>
      end
      object DBGrid3: TDBGrid
        Left = 664
        Top = 328
        Width = 225
        Height = 89
        DataSource = MainData.s_vosn
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Visible = False
      end
      object Button2: TButton
        Left = 660
        Top = 181
        Width = 55
        Height = 22
        Caption = 'Step 02'
        TabOrder = 3
        Visible = False
        OnClick = Button2Click
      end
      object Button4: TButton
        Left = 660
        Top = 205
        Width = 55
        Height = 25
        Caption = 'Step 03'
        TabOrder = 4
        Visible = False
        OnClick = Button4Click
      end
      object Button6: TButton
        Left = 716
        Top = 47
        Width = 170
        Height = 25
        Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1076#1072#1085#1080#1077' '#1085#1072' '#1088#1072#1089#1095#1077#1090
        TabOrder = 5
        OnClick = Button6Click
      end
      object Button3: TButton
        Left = 660
        Top = 154
        Width = 55
        Height = 25
        Caption = 'Step 01'
        TabOrder = 6
        Visible = False
        OnClick = Button3Click
      end
      object Button7: TButton
        Left = 716
        Top = 77
        Width = 171
        Height = 25
        Caption = #1056#1072#1089#1095#1080#1090#1072#1090#1100' '#1074#1086#1079#1074#1099#1096#1077#1085#1080#1103
        TabOrder = 7
        OnClick = Button7Click
      end
      object Button1: TButton
        Left = 660
        Top = 232
        Width = 55
        Height = 25
        Caption = 'Step 04'
        TabOrder = 8
        Visible = False
        OnClick = Button1Click
      end
      object Button5: TButton
        Left = 660
        Top = 259
        Width = 55
        Height = 25
        Caption = 'Step 05'
        TabOrder = 9
        Visible = False
        OnClick = Button5Click
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1042#1077#1076#1086#1084#1086#1089#1090#1100' '#1074#1086#1079#1074#1099#1096#1077#1085#1080#1081
      ImageIndex = 1
      object Bevel34: TBevel
        Left = 650
        Top = 40
        Width = 39
        Height = 26
        Shape = bsFrame
      end
      object Bevel33: TBevel
        Left = 615
        Top = 40
        Width = 37
        Height = 27
        Shape = bsFrame
      end
      object Bevel28: TBevel
        Left = 491
        Top = 4
        Width = 85
        Height = 62
        Shape = bsFrame
      end
      object Bevel24: TBevel
        Left = 339
        Top = 4
        Width = 47
        Height = 62
        Shape = bsFrame
      end
      object Bevel23: TBevel
        Left = 298
        Top = 4
        Width = 43
        Height = 62
        Shape = bsFrame
      end
      object Bevel17: TBevel
        Left = 170
        Top = 38
        Width = 43
        Height = 28
        Shape = bsFrame
      end
      object Bevel16: TBevel
        Left = 134
        Top = 38
        Width = 38
        Height = 28
        Shape = bsFrame
      end
      object Bevel22: TBevel
        Left = 247
        Top = 4
        Width = 53
        Height = 62
        Shape = bsFrame
      end
      object Bevel15: TBevel
        Left = 134
        Top = 4
        Width = 115
        Height = 36
        Shape = bsFrame
      end
      object Bevel14: TBevel
        Left = 211
        Top = 38
        Width = 38
        Height = 28
        Shape = bsFrame
      end
      object Bevel18: TBevel
        Left = 98
        Top = 38
        Width = 38
        Height = 28
        Shape = bsFrame
      end
      object Bevel19: TBevel
        Left = 57
        Top = 38
        Width = 43
        Height = 28
        Shape = bsFrame
      end
      object Bevel20: TBevel
        Left = 8
        Top = 38
        Width = 51
        Height = 28
        Shape = bsFrame
      end
      object Bevel21: TBevel
        Left = 8
        Top = 4
        Width = 128
        Height = 36
        Shape = bsFrame
      end
      object Label15: TLabel
        Left = 28
        Top = 45
        Width = 19
        Height = 16
        Caption = #1050#1052
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label16: TLabel
        Left = 113
        Top = 42
        Width = 9
        Height = 20
        Caption = '+'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label17: TLabel
        Left = 146
        Top = 16
        Width = 90
        Height = 16
        Caption = #1050#1086#1085#1077#1094' '#1082#1088#1080#1074#1086#1081
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label21: TLabel
        Left = 58
        Top = 45
        Width = 40
        Height = 16
        Caption = #1055#1080#1082#1077#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label22: TLabel
        Left = 23
        Top = 15
        Width = 100
        Height = 16
        Caption = #1053#1072#1095#1072#1083#1086' '#1082#1088#1080#1074#1086#1081
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label23: TLabel
        Left = 300
        Top = 22
        Width = 41
        Height = 32
        Caption = #1055#1086#1074#1086'- '#1088#1086#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label24: TLabel
        Left = 248
        Top = 24
        Width = 51
        Height = 32
        Caption = #1056#1072#1076#1080#1091#1089'         '#1084
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label25: TLabel
        Left = 144
        Top = 45
        Width = 19
        Height = 16
        Caption = #1050#1052
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label27: TLabel
        Left = 225
        Top = 42
        Width = 9
        Height = 20
        Caption = '+'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label28: TLabel
        Left = 172
        Top = 45
        Width = 40
        Height = 16
        Caption = #1055#1080#1082#1077#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label18: TLabel
        Left = 340
        Top = 20
        Width = 47
        Height = 32
        Caption = 'h '#1090#1077#1082#1091'- '#1097#1077#1077
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Bevel25: TBevel
        Left = 384
        Top = 4
        Width = 109
        Height = 38
        Shape = bsFrame
      end
      object Label19: TLabel
        Left = 386
        Top = 8
        Width = 103
        Height = 32
        Caption = #1044#1086#1087#1091#1089#1082#1072#1077#1084#1099#1077' '#1089#1082#1086#1088#1086#1089#1090#1080', V max'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Bevel26: TBevel
        Left = 437
        Top = 40
        Width = 56
        Height = 26
        Shape = bsFrame
      end
      object Bevel27: TBevel
        Left = 384
        Top = 40
        Width = 55
        Height = 26
        Shape = bsFrame
      end
      object Label20: TLabel
        Left = 397
        Top = 45
        Width = 33
        Height = 16
        Caption = #1087#1072#1089#1089'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label26: TLabel
        Left = 450
        Top = 45
        Width = 33
        Height = 16
        Caption = #1075#1088#1091#1079'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label29: TLabel
        Left = 495
        Top = 3
        Width = 77
        Height = 39
        Caption = #1055#1088#1080#1074#1077#1076#1077#1085#1085#1099#1077' '#1089#1082#1086#1088#1086#1089#1090#1080'  '#1087#1086#1077#1079#1076#1086#1087#1086#1090#1086#1082#1086#1074
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Bevel29: TBevel
        Left = 491
        Top = 40
        Width = 44
        Height = 27
        Shape = bsFrame
      end
      object Bevel30: TBevel
        Left = 533
        Top = 40
        Width = 43
        Height = 27
        Shape = bsFrame
      end
      object Label30: TLabel
        Left = 495
        Top = 45
        Width = 35
        Height = 16
        Caption = #1086#1073#1097#1100'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label31: TLabel
        Left = 538
        Top = 45
        Width = 33
        Height = 16
        Caption = #1075#1088#1091#1079'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Bevel31: TBevel
        Left = 574
        Top = 4
        Width = 115
        Height = 62
        Shape = bsFrame
      end
      object Label32: TLabel
        Left = 589
        Top = 4
        Width = 88
        Height = 32
        Caption = #1056#1072#1089#1095#1080#1090#1072#1085#1085#1099#1077' '#1074#1086#1079#1074#1099#1096#1077#1085#1080#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label33: TLabel
        Left = 617
        Top = 45
        Width = 33
        Height = 16
        Caption = #1075#1088#1091#1079'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label34: TLabel
        Left = 578
        Top = 45
        Width = 33
        Height = 16
        Caption = #1087#1072#1089#1089'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label35: TLabel
        Left = 657
        Top = 45
        Width = 26
        Height = 16
        Caption = #1087#1086#1090'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Bevel32: TBevel
        Left = 574
        Top = 40
        Width = 43
        Height = 27
        Shape = bsFrame
      end
      object Bevel35: TBevel
        Left = 687
        Top = 4
        Width = 187
        Height = 62
        Shape = bsFrame
      end
      object Label36: TLabel
        Left = 699
        Top = 7
        Width = 148
        Height = 13
        Caption = #1050#1086#1088#1088#1077#1082#1090#1080#1088#1086#1074#1072#1085#1085#1099#1077' '#1079#1085#1072#1095#1077#1085#1080#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Bevel36: TBevel
        Left = 687
        Top = 22
        Width = 104
        Height = 45
        Shape = bsFrame
      end
      object Bevel37: TBevel
        Left = 789
        Top = 22
        Width = 85
        Height = 44
        Shape = bsFrame
      end
      object Bevel38: TBevel
        Left = 687
        Top = 40
        Width = 54
        Height = 26
        Shape = bsFrame
      end
      object Bevel39: TBevel
        Left = 739
        Top = 40
        Width = 52
        Height = 26
        Shape = bsFrame
      end
      object Bevel40: TBevel
        Left = 789
        Top = 40
        Width = 43
        Height = 26
        Shape = bsFrame
      end
      object Bevel41: TBevel
        Left = 830
        Top = 40
        Width = 44
        Height = 26
        Shape = bsFrame
      end
      object Label37: TLabel
        Left = 697
        Top = 24
        Width = 87
        Height = 16
        Caption = #1044#1086#1087' '#1089#1082#1086#1088#1086#1089#1090#1100
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label39: TLabel
        Left = 697
        Top = 44
        Width = 33
        Height = 16
        Caption = #1087#1072#1089#1089'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label38: TLabel
        Left = 746
        Top = 44
        Width = 33
        Height = 16
        Caption = #1075#1088#1091#1079'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label40: TLabel
        Left = 790
        Top = 23
        Width = 82
        Height = 16
        Caption = #1074#1086#1079#1074#1099#1096#1077#1085#1080#1077
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label41: TLabel
        Left = 834
        Top = 44
        Width = 33
        Height = 16
        Caption = #1075#1088#1091#1079'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label42: TLabel
        Left = 793
        Top = 44
        Width = 33
        Height = 16
        Caption = #1087#1072#1089#1089'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Bevel42: TBevel
        Left = 872
        Top = 4
        Width = 42
        Height = 62
        Shape = bsFrame
      end
      object Label43: TLabel
        Left = 874
        Top = 7
        Width = 37
        Height = 52
        Caption = #1042#1086#1079'- '#1074#1099'- '#1096#1077'- '#1085#1080#1077' '#1080#1090#1086#1075'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object DBGrid4: TDBGrid
        Left = 8
        Top = 64
        Width = 929
        Height = 377
        DataSource = MainData.s_vedomost
        Options = [dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'id_ved'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'id_zadan'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'id_uch'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'id_put'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'km_nach'
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pk_nach'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pl_nach'
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'km_konc'
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pk_konc'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pl_konc'
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'dlin_kriv'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'r'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'napravl'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'h_tek'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'v_max_pass'
            Width = 53
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'v_max_gr'
            Width = 53
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'v_priv'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'v_p_gr'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'h_pass'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'h_gr'
            Width = 34
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'h_pot'
            Width = 37
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'v_max_p_k'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'v_max_gr_k'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'h_pass_kor'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'h_gr_kor'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'h_itog'
            Width = 40
            Visible = True
          end>
      end
    end
  end
end
