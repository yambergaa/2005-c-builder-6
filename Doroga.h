//---------------------------------------------------------------------------

#ifndef DorogaH
#define DorogaH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include <DBGrids.hpp>
#include <Grids.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TfDoroga : public TForm
{
__published:	// IDE-managed Components
        TPageControl *PageControl1;
        TTabSheet *TabSheet1;
        TTabSheet *TabSheet3;
        TTabSheet *TabSheet4;
        TDBGrid *dkm_puchGrid;
        TDBGrid *planGrid;
        TDBGrid *DBGrid4;
        TBevel *Bevel1;
        TBevel *Bevel2;
        TBevel *Bevel3;
        TLabel *Label1;
        TLabel *Label2;
        TBevel *Bevel4;
        TBevel *Bevel5;
        TBevel *Bevel6;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label6;
        TLabel *Label7;
        TLabel *Label8;
        TBevel *Bevel7;
        TLabel *Label9;
        TBevel *Bevel8;
        TBevel *Bevel9;
        TBevel *Bevel10;
        TLabel *Label10;
        TBevel *Bevel11;
        TLabel *Label11;
        TBevel *Bevel12;
        TBevel *Bevel13;
        TLabel *Label12;
        TBevel *Bevel14;
        TLabel *Label13;
        TLabel *Label14;
        TLabel *Label15;
        TBevel *Bevel15;
        TBevel *Bevel16;
        TLabel *Label16;
        TLabel *Label17;
        TLabel *Label18;
        TBevel *Bevel17;
        TBevel *Bevel18;
        TBevel *Bevel19;
        TLabel *Label19;
        TLabel *Label20;
        TBevel *Bevel21;
        TBevel *Bevel22;
        TLabel *Label21;
        TLabel *Label22;
        TBevel *Bevel20;
        TBevel *Bevel26;
        TLabel *Label26;
        TLabel *Label27;
        TBevel *Bevel27;
        TLabel *Label28;
        TLabel *Label29;
        TBevel *Bevel29;
        TLabel *Label30;
        TBevel *Bevel30;
        TLabel *Label31;
        TBevel *Bevel31;
        TBevel *Bevel32;
        TLabel *Label32;
        TLabel *Label33;
        TLabel *Label34;
        TBevel *Bevel33;
        TLabel *Label25;
        TBevel *Bevel23;
        TLabel *Label37;
        TBevel *Bevel24;
        TLabel *Label38;
        TBevel *Bevel25;
        TLabel *Label39;
        TLabel *Label41;
        TBevel *Bevel28;
        TLabel *Label42;
        TBevel *Bevel38;
        TLabel *Label43;
        TBevel *Bevel39;
        TLabel *Label44;
        TBevel *Bevel40;
        TBevel *Bevel41;
        TBevel *Bevel42;
        TBevel *Bevel43;
        TBevel *Bevel44;
        TLabel *Label45;
        TLabel *Label46;
        TLabel *Label47;
        TTabSheet *TabSheet5;
        TDBGrid *DBGrid1;
        TLabel *Label48;
        TLabel *Label49;
        TLabel *Label50;
        TBevel *Bevel45;
        TBevel *Bevel46;
        TLabel *Label51;
        TBevel *Bevel47;
        TBevel *Bevel48;
        TLabel *Label52;
        TLabel *Label53;
        TLabel *Label54;
        TLabel *Label55;
        TBevel *Bevel49;
        TBevel *Bevel50;
        TBevel *Bevel51;
        TBevel *Bevel52;
        TBevel *Bevel53;
        TBevel *Bevel54;
        TBevel *Bevel55;
        TLabel *Label56;
        TLabel *Label57;
        TLabel *Label58;
private:	// User declarations
public:		// User declarations
        __fastcall TfDoroga(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfDoroga *fDoroga;
//---------------------------------------------------------------------------
#endif
