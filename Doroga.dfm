object fDoroga: TfDoroga
  Left = 169
  Top = 108
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1044#1086#1088#1086#1075#1072
  ClientHeight = 489
  ClientWidth = 790
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel10: TBevel
    Left = 10
    Top = 24
    Width = 133
    Height = 26
    Shape = bsFrame
  end
  object Bevel9: TBevel
    Left = 64
    Top = 48
    Width = 48
    Height = 26
    Shape = bsFrame
  end
  object Bevel8: TBevel
    Left = 10
    Top = 48
    Width = 56
    Height = 26
    Shape = bsFrame
  end
  object Label9: TLabel
    Left = 122
    Top = 50
    Width = 9
    Height = 20
    Caption = '+'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Bevel7: TBevel
    Left = 110
    Top = 48
    Width = 33
    Height = 26
    Shape = bsFrame
  end
  object Label8: TLabel
    Left = 68
    Top = 53
    Width = 40
    Height = 16
    Caption = #1055#1080#1082#1077#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label7: TLabel
    Left = 35
    Top = 53
    Width = 19
    Height = 16
    Caption = #1050#1052
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label6: TLabel
    Left = 31
    Top = 29
    Width = 100
    Height = 16
    Caption = #1053#1072#1095#1072#1083#1086' '#1082#1088#1080#1074#1086#1081
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 790
    Height = 489
    ActivePage = TabSheet1
    Align = alClient
    TabIndex = 0
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #1044#1077#1081#1089#1090#1074#1080#1090#1077#1083#1100#1085#1099#1081' '#1082#1080#1083#1086#1084#1072#1090#1088#1072#1078' '#1087#1086#1076#1091#1095#1072#1089#1090#1082#1086#1074
      object Bevel31: TBevel
        Left = 539
        Top = 25
        Width = 64
        Height = 20
        Shape = bsFrame
      end
      object Label33: TLabel
        Left = 491
        Top = 27
        Width = 40
        Height = 16
        Caption = #1055#1080#1082#1077#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Bevel32: TBevel
        Left = 418
        Top = 3
        Width = 185
        Height = 24
        Shape = bsFrame
      end
      object Label32: TLabel
        Left = 447
        Top = 7
        Width = 120
        Height = 16
        Caption = #1050#1086#1085#1077#1094' '#1087#1086#1076#1091#1095#1072#1089#1090#1082#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label34: TLabel
        Left = 443
        Top = 27
        Width = 19
        Height = 16
        Caption = #1050#1052
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Bevel33: TBevel
        Left = 418
        Top = 25
        Width = 63
        Height = 20
        Shape = bsFrame
      end
      object Bevel26: TBevel
        Left = 235
        Top = 3
        Width = 185
        Height = 24
        Shape = bsFrame
      end
      object Bevel27: TBevel
        Left = 235
        Top = 25
        Width = 63
        Height = 20
        Shape = bsFrame
      end
      object Bevel29: TBevel
        Left = 357
        Top = 25
        Width = 63
        Height = 20
        Shape = bsFrame
      end
      object Label29: TLabel
        Left = 382
        Top = 26
        Width = 9
        Height = 20
        Caption = '+'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label28: TLabel
        Left = 308
        Top = 27
        Width = 40
        Height = 16
        Caption = #1055#1080#1082#1077#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label27: TLabel
        Left = 260
        Top = 27
        Width = 19
        Height = 16
        Caption = #1050#1052
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label26: TLabel
        Left = 256
        Top = 7
        Width = 130
        Height = 16
        Caption = #1053#1072#1095#1072#1083#1086' '#1087#1086#1076#1091#1095#1072#1089#1090#1082#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Bevel30: TBevel
        Left = 142
        Top = 3
        Width = 95
        Height = 43
        Shape = bsFrame
      end
      object Label30: TLabel
        Left = 181
        Top = 7
        Width = 20
        Height = 32
        Caption = #8470' '#1087'/'#1091
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label31: TLabel
        Left = 565
        Top = 26
        Width = 9
        Height = 20
        Caption = '+'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object dkm_puchGrid: TDBGrid
        Left = 141
        Top = 43
        Width = 465
        Height = 414
        DataSource = MainData.s_dkm_puch
        Options = [dgEditing, dgIndicator, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'id_uch'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'id_put'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'num_p_uch'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'km_nach'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pk_nach'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pl_nach'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'km_konc'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pk_konc'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pl_konc'
            Width = 60
            Visible = True
          end>
      end
    end
    object TabSheet3: TTabSheet
      Caption = #1055#1083#1072#1085' '#1087#1091#1090#1080
      ImageIndex = 2
      object Bevel24: TBevel
        Left = 7
        Top = 24
        Width = 36
        Height = 50
        Shape = bsFrame
      end
      object Bevel22: TBevel
        Left = 576
        Top = 24
        Width = 118
        Height = 32
        Shape = bsFrame
      end
      object Bevel2: TBevel
        Left = 41
        Top = 24
        Width = 120
        Height = 32
        Shape = bsFrame
      end
      object Bevel15: TBevel
        Left = 692
        Top = 24
        Width = 67
        Height = 50
        Shape = bsFrame
      end
      object Bevel6: TBevel
        Left = 128
        Top = 54
        Width = 33
        Height = 20
        Shape = bsFrame
      end
      object Label17: TLabel
        Left = 579
        Top = 23
        Width = 116
        Height = 32
        Caption = #1050#1088#1091#1090#1080#1079#1085#1072' '#1086#1090#1074#1086#1076#1072' '#1082#1088#1080#1074#1099#1093
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Bevel14: TBevel
        Left = 159
        Top = 54
        Width = 43
        Height = 20
        Shape = bsFrame
      end
      object Bevel1: TBevel
        Left = 41
        Top = 54
        Width = 43
        Height = 20
        Shape = bsFrame
      end
      object Bevel5: TBevel
        Left = 82
        Top = 54
        Width = 48
        Height = 20
        Shape = bsFrame
      end
      object Bevel16: TBevel
        Left = 379
        Top = 24
        Width = 67
        Height = 50
        Shape = bsFrame
      end
      object Bevel17: TBevel
        Left = 444
        Top = 24
        Width = 134
        Height = 50
        Shape = bsFrame
      end
      object Label16: TLabel
        Left = 380
        Top = 24
        Width = 66
        Height = 48
        Caption = #1059#1075#1086#1083' '#1087#1086#1074#1086#1088#1086#1090#1072' '#1075#1088','#1084#1080#1085
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Bevel4: TBevel
        Left = 323
        Top = 24
        Width = 58
        Height = 50
        Shape = bsFrame
      end
      object Bevel13: TBevel
        Left = 159
        Top = 24
        Width = 120
        Height = 32
        Shape = bsFrame
      end
      object Label13: TLabel
        Left = 170
        Top = 56
        Width = 19
        Height = 16
        Caption = #1050#1052
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label12: TLabel
        Left = 171
        Top = 29
        Width = 90
        Height = 16
        Caption = #1050#1086#1085#1077#1094' '#1082#1088#1080#1074#1086#1081
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Bevel12: TBevel
        Left = 200
        Top = 54
        Width = 48
        Height = 20
        Shape = bsFrame
      end
      object Label11: TLabel
        Left = 204
        Top = 56
        Width = 40
        Height = 16
        Caption = #1055#1080#1082#1077#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Bevel11: TBevel
        Left = 246
        Top = 54
        Width = 33
        Height = 20
        Shape = bsFrame
      end
      object Label10: TLabel
        Left = 258
        Top = 53
        Width = 9
        Height = 20
        Caption = '+'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label5: TLabel
        Left = 140
        Top = 54
        Width = 9
        Height = 20
        Caption = '+'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label4: TLabel
        Left = 86
        Top = 56
        Width = 40
        Height = 16
        Caption = #1055#1080#1082#1077#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label3: TLabel
        Left = 326
        Top = 32
        Width = 51
        Height = 32
        Caption = #1056#1072#1076#1080#1091#1089'         '#1084
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label2: TLabel
        Left = 53
        Top = 56
        Width = 19
        Height = 16
        Caption = #1050#1052
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label1: TLabel
        Left = 49
        Top = 29
        Width = 100
        Height = 16
        Caption = #1053#1072#1095#1072#1083#1086' '#1082#1088#1080#1074#1086#1081
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Bevel3: TBevel
        Left = 277
        Top = 24
        Width = 48
        Height = 50
        Shape = bsFrame
      end
      object Label14: TLabel
        Left = 280
        Top = 28
        Width = 41
        Height = 32
        Caption = #1055#1086#1074#1086'- '#1088#1086#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label15: TLabel
        Left = 694
        Top = 32
        Width = 63
        Height = 32
        Caption = #1042#1086#1079#1074#1099#1096#1077'- '#1085#1080#1077',  '#1084
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label18: TLabel
        Left = 450
        Top = 23
        Width = 125
        Height = 32
        Caption = #1044#1083#1080#1085#1099' '#1087#1077#1088#1077#1093#1086#1076#1085#1099#1093' '#1082#1088#1080#1074#1099#1093', '#1084
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Bevel18: TBevel
        Left = 444
        Top = 54
        Width = 68
        Height = 20
        Shape = bsFrame
      end
      object Bevel19: TBevel
        Left = 510
        Top = 54
        Width = 68
        Height = 20
        Shape = bsFrame
      end
      object Label19: TLabel
        Left = 451
        Top = 56
        Width = 48
        Height = 16
        Caption = #1087#1077#1088#1074#1086#1081
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label20: TLabel
        Left = 520
        Top = 56
        Width = 47
        Height = 16
        Caption = #1074#1090#1086#1088#1086#1081
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Bevel21: TBevel
        Left = 576
        Top = 54
        Width = 60
        Height = 20
        Shape = bsFrame
      end
      object Label21: TLabel
        Left = 581
        Top = 54
        Width = 48
        Height = 16
        Caption = #1087#1077#1088#1074#1086#1081
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label22: TLabel
        Left = 638
        Top = 54
        Width = 47
        Height = 16
        Caption = #1074#1090#1086#1088#1086#1081
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Bevel20: TBevel
        Left = 634
        Top = 54
        Width = 60
        Height = 20
        Shape = bsFrame
      end
      object Label37: TLabel
        Left = 14
        Top = 31
        Width = 20
        Height = 32
        Caption = #8470' '#1087'/'#1091
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object planGrid: TDBGrid
        Left = 7
        Top = 72
        Width = 769
        Height = 385
        DataSource = MainData.s_plan
        Options = [dgEditing, dgIndicator, dgColLines, dgRowLines, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'id_uch'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'id_put'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'n_p_uch'
            Width = 20
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'km_nach'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pk_nach'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pl_nach'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'km_konc'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pk_konc'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pl_konc'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'povorot'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'r'
            Width = 54
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ugol'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'dlin_kriv'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'dlin_1per'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'dlin_2per'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'krut_otv_1'
            Width = 57
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'krut_otv_2'
            Width = 57
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'h'
            Visible = True
          end>
      end
    end
    object TabSheet4: TTabSheet
      Caption = #1056#1072#1079#1076#1077#1083#1100#1085#1099#1077' '#1087#1091#1085#1082#1090#1099
      ImageIndex = 3
      object Bevel39: TBevel
        Left = 427
        Top = 73
        Width = 64
        Height = 21
        Shape = bsFrame
      end
      object Bevel28: TBevel
        Left = 366
        Top = 41
        Width = 185
        Height = 34
        Shape = bsFrame
      end
      object Bevel41: TBevel
        Left = 295
        Top = 41
        Width = 73
        Height = 53
        Shape = bsFrame
      end
      object Bevel40: TBevel
        Left = 489
        Top = 73
        Width = 62
        Height = 21
        Shape = bsFrame
      end
      object Bevel38: TBevel
        Left = 366
        Top = 73
        Width = 63
        Height = 21
        Shape = bsFrame
      end
      object Bevel23: TBevel
        Left = 82
        Top = 5
        Width = 215
        Height = 89
        Shape = bsFrame
      end
      object Label25: TLabel
        Left = 317
        Top = 49
        Width = 20
        Height = 32
        Caption = #8470' '#1087'/'#1091
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label38: TLabel
        Left = 101
        Top = 29
        Width = 180
        Height = 32
        Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1088#1072#1079#1076#1077#1083#1100#1085#1099#1093' '#1087#1091#1085#1082#1090#1086#1074
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Bevel25: TBevel
        Left = 295
        Top = 5
        Width = 256
        Height = 38
        Shape = bsFrame
      end
      object Label39: TLabel
        Left = 311
        Top = 14
        Width = 193
        Height = 16
        Caption = #1055#1086#1083#1086#1078#1077#1085#1080#1077' '#1086#1089#1080' '#1055#1047' / '#1087#1086#1089#1090#1072' '#1069#1062
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label41: TLabel
        Left = 400
        Top = 41
        Width = 113
        Height = 32
        Caption = #1044#1077#1081#1089#1090#1074#1080#1090#1077#1083#1100#1085#1099#1081' '#1082#1080#1083#1086#1084#1077#1090#1088#1072#1078
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label42: TLabel
        Left = 390
        Top = 75
        Width = 19
        Height = 16
        Caption = #1050#1052
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label43: TLabel
        Left = 437
        Top = 76
        Width = 40
        Height = 16
        Caption = #1055#1080#1082#1077#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label44: TLabel
        Left = 515
        Top = 73
        Width = 9
        Height = 20
        Caption = '+'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Bevel42: TBevel
        Left = 549
        Top = 5
        Width = 151
        Height = 38
        Shape = bsFrame
      end
      object Bevel43: TBevel
        Left = 549
        Top = 41
        Width = 77
        Height = 53
        Shape = bsFrame
      end
      object Bevel44: TBevel
        Left = 624
        Top = 41
        Width = 76
        Height = 53
        Shape = bsFrame
      end
      object Label45: TLabel
        Left = 550
        Top = 6
        Width = 149
        Height = 32
        Caption = #1056#1072#1089#1089#1090#1086#1103#1085#1080#1077' '#1076#1086' '#1089#1090#1088#1077#1083#1086'- '#1095#1085#1099#1093' '#1087#1077#1088#1077#1074#1086#1076#1086#1074', '#1084
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label46: TLabel
        Left = 558
        Top = 60
        Width = 60
        Height = 16
        Caption = #1074#1093#1086#1076#1085#1086#1075#1086
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label47: TLabel
        Left = 627
        Top = 60
        Width = 69
        Height = 16
        Caption = #1074#1099#1093#1086#1076#1085#1086#1075#1086
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object DBGrid4: TDBGrid
        Left = 81
        Top = 91
        Width = 638
        Height = 358
        DataSource = MainData.s_rpunkt
        Options = [dgEditing, dgIndicator, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'name'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'id_uch'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'id_put'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'num_p_uch'
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'km'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pk'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pl'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'rast_vhod'
            Width = 73
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'rast_vih'
            Width = 73
            Visible = True
          end>
      end
    end
    object TabSheet5: TTabSheet
      Caption = #1044#1086#1087#1091#1089#1082#1072#1077#1084#1099#1077' '#1089#1082#1086#1088#1086#1089#1090#1080
      ImageIndex = 4
      object Bevel50: TBevel
        Left = 454
        Top = 38
        Width = 62
        Height = 20
        Shape = bsFrame
      end
      object Bevel51: TBevel
        Left = 393
        Top = 38
        Width = 63
        Height = 20
        Shape = bsFrame
      end
      object Bevel49: TBevel
        Left = 332
        Top = 8
        Width = 184
        Height = 32
        Shape = bsFrame
      end
      object Bevel52: TBevel
        Left = 332
        Top = 38
        Width = 63
        Height = 20
        Shape = bsFrame
      end
      object Bevel47: TBevel
        Left = 210
        Top = 38
        Width = 63
        Height = 20
        Shape = bsFrame
      end
      object Bevel45: TBevel
        Left = 137
        Top = 8
        Width = 197
        Height = 32
        Shape = bsFrame
      end
      object Bevel48: TBevel
        Left = 137
        Top = 38
        Width = 75
        Height = 20
        Shape = bsFrame
      end
      object Bevel46: TBevel
        Left = 271
        Top = 38
        Width = 63
        Height = 20
        Shape = bsFrame
      end
      object Label48: TLabel
        Left = 172
        Top = 7
        Width = 141
        Height = 32
        Caption = #1053#1072#1095#1072#1083#1086' '#1086#1075#1088#1072#1085#1080#1095#1077#1085#1080#1103' '#1089#1082#1086#1088#1086#1089#1090#1080
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label49: TLabel
        Left = 170
        Top = 40
        Width = 19
        Height = 16
        Caption = #1050#1052
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label50: TLabel
        Left = 296
        Top = 38
        Width = 9
        Height = 20
        Caption = '+'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label51: TLabel
        Left = 222
        Top = 40
        Width = 40
        Height = 16
        Caption = #1055#1080#1082#1077#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label52: TLabel
        Left = 480
        Top = 38
        Width = 9
        Height = 20
        Caption = '+'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label53: TLabel
        Left = 406
        Top = 40
        Width = 40
        Height = 16
        Caption = #1055#1080#1082#1077#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label54: TLabel
        Left = 354
        Top = 40
        Width = 19
        Height = 16
        Caption = #1050#1052
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label55: TLabel
        Left = 358
        Top = 7
        Width = 131
        Height = 32
        Caption = #1050#1086#1085#1077#1094' '#1086#1075#1088#1072#1085#1080#1095#1077#1085#1080#1103' '#1089#1082#1086#1088#1086#1089#1090#1080
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Bevel53: TBevel
        Left = 514
        Top = 8
        Width = 125
        Height = 50
        Shape = bsFrame
      end
      object Bevel54: TBevel
        Left = 514
        Top = 38
        Width = 64
        Height = 20
        Shape = bsFrame
      end
      object Bevel55: TBevel
        Left = 576
        Top = 38
        Width = 63
        Height = 20
        Shape = bsFrame
      end
      object Label56: TLabel
        Left = 536
        Top = 7
        Width = 92
        Height = 32
        Caption = #1044#1086#1087#1091#1089#1082#1072#1077#1084#1099#1077' '#1089#1082#1086#1088#1086#1089#1090#1080
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
      object Label57: TLabel
        Left = 528
        Top = 40
        Width = 33
        Height = 16
        Caption = #1087#1072#1089#1089'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label58: TLabel
        Left = 586
        Top = 40
        Width = 33
        Height = 16
        Caption = #1075#1088#1091#1079'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object DBGrid1: TDBGrid
        Left = 136
        Top = 56
        Width = 521
        Height = 401
        DataSource = MainData.s_vdop
        Options = [dgEditing, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'id_uch'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'id_put'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'km_nach'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pk_nach'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pl_nach'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'km_konc'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pk_konc'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pl_konc'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'v_dop_pass'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'v_dop_gr'
            Width = 60
            Visible = True
          end>
      end
    end
  end
end
